cmake_minimum_required(VERSION 3.0)
project(binary_tree)

set(HEADERS
    binary_tree/construct.h
    binary_tree/node.h
    binary_tree/types.h
)

add_library(${PROJECT_NAME} INTERFACE)
target_include_directories(${PROJECT_NAME} INTERFACE .)
target_compile_features(${PROJECT_NAME} INTERFACE cxx_std_11)
