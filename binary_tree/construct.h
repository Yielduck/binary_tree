#pragma once
#include "node.h"
#include <memory>
#include <queue>
namespace binary_tree
{

template<typename Callable>
/* 
 * Callable must implement
 * operator()(idx_iter begin, idx_iter end, idx_t const current) [const] noexcept -> idx_iter
 *
 * this method operates on indices range, owned by `construct' function, and is used to divide
 * any [begin, end) range satisfying `(end - begin) > 1', into non-empty ranges [begin, middle) and [middle, end)
 * 
 * Callable::operator() must return `middle' which satisfies `begin < middle < end'
 * 
 * resulting partitioning defines binary tree: [begin, middle) range serves as the left child, [middle, end) forms the right child
 * `current' is index of current node being processed as parent; it may be used for storing data acquired while partitioning
 */
std::unique_ptr<node[]> construct(idx_t const distance, Callable const &partition)
{
    std::unique_ptr<node[]> tree(new node[2 * distance - 1]);
    std::unique_ptr<idx_t[]> idx(new idx_t[distance]);
    for(idx_t i = 0; i < distance; ++i)
        idx[i] = i;

    struct entry
    {
        idx_iter begin;
        idx_iter end;
        idx_t current;
    };
    std::queue<entry> queue;
    idx_t nodecount = 0;

    queue.push({idx.get(), idx.get() + distance, nodecount++});
    while(!queue.empty())
    {
        entry const ent         = queue.front();
        idx_iter const ibegin   = ent.begin;
        idx_iter const iend     = ent.end;
        idx_t const curr        = ent.current;
        //auto const [ibegin, iend, curr] = queue.front(); // c++17
        queue.pop();

        // the `(i << 1) >> 1' thing sets last bit of a number to 0
        if(iend - ibegin == 1)
        {
            tree[curr].state = node::leaf;
            tree[curr].info  = (*ibegin << 1) >> 1; // index in [begin, end) range
        }
        else
        {
            tree[curr].state = node::parent;
            tree[curr].info  = (nodecount << 1) >> 1; // left child index

            idx_iter const imiddle = partition(ibegin, iend, curr);
            assert(ibegin < imiddle && imiddle < iend);
            queue.push({ibegin, imiddle, nodecount++});
            queue.push({imiddle, iend, nodecount++});
        }
    }
    return tree;
}

} // namespace binary_tree
