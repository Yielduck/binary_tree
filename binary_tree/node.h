#pragma once
#include "types.h"
#include <cassert>
namespace binary_tree
{

struct node
{
    static constexpr idx_t parent = 0u;
    static constexpr idx_t leaf   = 1u;

    idx_t info  : 8u * sizeof(idx_t) - 1u;
    idx_t state :                      1u;

    idx_t left  ()  const noexcept {assert(state == parent); return info;}
    idx_t right ()  const noexcept {assert(state == parent); return info + 1u;}

    idx_t idx   ()  const noexcept {assert(state == leaf  ); return info;}
};

} // namespace binary_tree
