#pragma once
#include <cstdint>
namespace binary_tree
{

using idx_t = std::uint32_t;
using idx_iter = idx_t *;

} // namespace binary_tree
