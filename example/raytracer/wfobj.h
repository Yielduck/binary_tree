#pragma once
#include <cstdio>
#include <cstdint>
#include <cassert>
#include <fstream>
#include <vector>
#include "vec3.h"

struct Mesh
{
    using uint = std::uint32_t;

    struct vertex
    {
        vec3 pos;
        //vec2 tex;
        vec3 norm;
    };

    std::vector<vertex> verts;
    std::vector<uint> inds;
};

inline Mesh importOBJ(char const * const filename)
{
    std::ifstream in(filename);
    if(!in.is_open())
        throw std::invalid_argument("can not find file " + std::string(filename));

    Mesh out;
    std::vector<vec3> pos;
    //std::vector<vec2> tex;
    std::vector<vec3> norm;

    std::string line;
    while(std::getline(in, line))
    {
        char const * const cstr = line.c_str();
        if(cstr[0] == 'v')
        {
            vec3 v;
            //vec2 vt;
            if(std::sscanf(cstr + 1, " %f %f %f", &v[0], &v[1], &v[2]) == 3)
                pos.push_back(v);
            //else if(std::sscanf(cstr + 1, "t %f %f", &vt[0], &vt[1]) == 2)
            //    tex.push_back(vt);
            else if(std::sscanf(cstr + 1, "n %f %f %f", &v[0], &v[1], &v[2]) == 3)
                norm.push_back(v);
            //else
            //    assert(0);
        }
        else if(cstr[0] == 'f')
        {
            Mesh::uint idx[3];
            Mesh::uint const vsize = static_cast<Mesh::uint>(out.verts.size());

            char const *cptr = cstr + 2;
            int eaten;
            while(std::sscanf(cptr, "%u/%u/%u%n", idx, idx + 1, idx + 2, &eaten) == 3)
            {
                out.verts.push_back({pos[idx[0] - 1], /*tex[idx[1] - 1], */norm[idx[2] - 1]});
                cptr += eaten;
            }

            Mesh::uint const vcount = static_cast<Mesh::uint>(out.verts.size() - vsize);
            assert(vcount != 0);
            for(Mesh::uint i = 1; i < vcount - 1; i++)
            {
                out.inds.push_back(vsize);
                out.inds.push_back(vsize + i);
                out.inds.push_back(vsize + i + 1);
            }
        }
    }
    return out;
}
