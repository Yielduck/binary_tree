#pragma once
#include "vec3.h"
struct Ray
{
    vec3 pos;
    vec3 dir;
};
