#pragma once
#include "vec3.h"
struct Color
{
    unsigned char r, g, b;
};
Color toRGB8(vec3 const fc, float const exposure) noexcept
{
    vec3 const c =
    {
        1.f - std::exp(-fc.x * exposure),
        1.f - std::exp(-fc.y * exposure),
        1.f - std::exp(-fc.z * exposure),
    };
    float const almost256 = 256.f - 1e-6f;
    return
    {
        static_cast<unsigned char>(c.x * almost256),
        static_cast<unsigned char>(c.y * almost256),
        static_cast<unsigned char>(c.z * almost256),
    };
}
