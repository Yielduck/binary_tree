#pragma once
#include <cmath>
struct vec3
{
    float x, y, z;

    // hello UB my old friend
    float &operator[](unsigned int const i)       noexcept {return *(&x + i);}
    float  operator[](unsigned int const i) const noexcept {return *(&x + i);}
};
inline constexpr vec3 operator+(vec3 const v1, vec3 const v2) noexcept
{
    return {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
}
inline constexpr vec3 operator-(vec3 const v1, vec3 const v2) noexcept
{
    return {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
}
inline constexpr vec3 operator*(vec3 const v1, vec3 const v2) noexcept
{
    return {v1.x * v2.x, v1.y * v2.y, v1.z * v2.z};
}
inline constexpr vec3 operator*(vec3 const v, float const f) noexcept
{
    return {v.x * f, v.y * f, v.z * f};
}
inline constexpr vec3 operator/(vec3 const v, float const f) noexcept
{
    return {v.x / f, v.y / f, v.z / f};
}
inline constexpr float dot(vec3 const v1, vec3 const v2) noexcept
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}
inline constexpr vec3 cross(vec3 const v1, vec3 const v2) noexcept
{
    return
    {
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x,
    };
}
inline constexpr float length(vec3 const &v) noexcept
{
    return std::sqrt(dot(v, v));
}
inline constexpr vec3 normalize(vec3 const &v) noexcept
{
    return v / length(v);
}
