#pragma once
#include "kdtree.h"
#include "ray_triangle_intersection.h"
#include "wfobj.h"
#include <optional>

struct KDMesh
{
    KDTree kdtree;
    std::vector<Triangle> triangle;
    std::vector<Triangle> normTriangle;

    KDMesh(Mesh const &mesh)
    {
        std::vector<KDTree::AABB> box;
        Mesh::uint const sz = static_cast<Mesh::uint>(mesh.inds.size() / 3);
        box.reserve(sz);
        triangle.reserve(sz);
        normTriangle.reserve(sz);
        for(Mesh::uint i = 0; i < sz; ++i)
        {
            vec3 const p[3] =
            {
                mesh.verts[mesh.inds[3 * i    ]].pos,
                mesh.verts[mesh.inds[3 * i + 1]].pos,
                mesh.verts[mesh.inds[3 * i + 2]].pos,
            };
            vec3 const pmin =
            {
                std::min(std::min(p[0][0], p[1][0]), p[2][0]),
                std::min(std::min(p[0][1], p[1][1]), p[2][1]),
                std::min(std::min(p[0][2], p[1][2]), p[2][2]),
            };
            vec3 const pmax =
            {
                std::max(std::max(p[0][0], p[1][0]), p[2][0]),
                std::max(std::max(p[0][1], p[1][1]), p[2][1]),
                std::max(std::max(p[0][2], p[1][2]), p[2][2]),
            };
            box.push_back({pmin, pmax});
            triangle.emplace_back(p[0], p[1], p[2]);
            normTriangle.emplace_back
            (
                mesh.verts[mesh.inds[3 * i    ]].norm,
                mesh.verts[mesh.inds[3 * i + 1]].norm,
                mesh.verts[mesh.inds[3 * i + 2]].norm
            );
        }
        kdtree = KDTree(box.begin(), box.end());
    }

    struct Hit
    {
        vec3 pos;
        vec3 norm;
    };

    std::optional<Hit> closestHit(Ray const ray) const noexcept
    {
        vec3 const invdir =
        {
            1.f / ray.dir[0],
            1.f / ray.dir[1],
            1.f / ray.dir[2],
        };
        auto const AABBtest = [=](KDTree::AABB const &aabb) noexcept
            -> std::pair<float, float>
        {
            float const t[] =
            {
                (aabb.pmin[0] - ray.pos[0]) * invdir[0],
                (aabb.pmin[1] - ray.pos[1]) * invdir[1],
                (aabb.pmin[2] - ray.pos[2]) * invdir[2],
                (aabb.pmax[0] - ray.pos[0]) * invdir[0],
                (aabb.pmax[1] - ray.pos[1]) * invdir[1],
                (aabb.pmax[2] - ray.pos[2]) * invdir[2],
            };
            float const tmin[] =
            {
                t[0] < t[3] ? t[0] : t[3],
                t[1] < t[4] ? t[1] : t[4],
                t[2] < t[5] ? t[2] : t[5],
            };
            float const tmax[] =
            {
                t[0] < t[3] ? t[3] : t[0],
                t[1] < t[4] ? t[4] : t[1],
                t[2] < t[5] ? t[5] : t[2],
            };
            return
            {
                std::max(tmin[0], std::max(tmin[1], tmin[2])),
                std::min(tmax[0], std::min(tmax[1], tmax[2])),
            };
        };

        float const tmin = 1e-4f;
        float const tmax = 1e5f;
        binary_tree::idx_t const sz = static_cast<binary_tree::idx_t>(triangle.size());
        float t = tmax;
        binary_tree::idx_t I = sz;
        Triangle::Barycentric coord = {0.f, 0.f};

        /*
        // bruteforce
        for(unsigned int i = 0; i < sz; ++i)
        {
            auto const [it, icoord] = intersect(ray, triangle[i]);
            if(it < t && icoord.valid() && it > tmin)
            {
                t = it;
                coord = icoord;
                I = i;
            }
        }
        */

        struct stack_entry
        {
            binary_tree::idx_t idx;
            float t;
        };
        stack_entry traverse_stack[16];
        traverse_stack[0] = {0, t};
        unsigned int stack_size = 1;
big_break:
        while(stack_size != 0)
        {
            stack_entry e = traverse_stack[--stack_size];
            if(e.t > t)
                continue;

            while(kdtree.tree[e.idx].state == binary_tree::node::parent)
            {
                binary_tree::node const node = kdtree.tree[e.idx];

                std::pair<float, float> const t1 = AABBtest(kdtree.aabb[node.left ()]);
                std::pair<float, float> const t2 = AABBtest(kdtree.aabb[node.right()]);
                bool const t1_good = t1.first <= t1.second && t1.first < t && t1.second > tmin;
                bool const t2_good = t2.first <= t2.second && t2.first < t && t2.second > tmin;
                if(t1_good && t2_good)
                {
                    e = t1.first < t2.first
                        ? stack_entry{node.left (), t1.first}
                        : stack_entry{node.right(), t2.first};
                    traverse_stack[stack_size++] = t1.first < t2.first
                        ? stack_entry{node.right(), t2.first}
                        : stack_entry{node.left (), t1.first};
                }
                else if(t1_good)
                    e = {node.left (), t1.first};
                else if(t2_good)
                    e = {node.right(), t2.first};
                else
                    goto big_break;
            }
            binary_tree::idx_t const i = kdtree.tree[e.idx].idx();
            auto const [it, icoord] = RayTriangleIntersection(ray, triangle[i]);
            if(it < t && icoord.valid() && it > tmin)
            {
                t = it;
                coord = icoord;
                I = i;
            }
        }
        return I == sz
            ? std::optional<Hit>()
            : std::optional<Hit>({ray.pos + ray.dir * t, normTriangle[I].interpolate(coord)});
    }
};
