#pragma once
#include "ray.h"
struct Camera
{
    float fov;
    float ratio;
    vec3 pos;
    vec3 up;
    vec3 at;

    // u and v are dimensionless screen coordinates
    // both belonging to [-1;1]
    Ray cast(float const u, float const v) const noexcept
    {
        vec3 const dir = normalize(at - pos);
        vec3 const right = normalize(cross(dir, up));
        vec3 const true_up = cross(right, dir);
        return
        {
            pos,
            normalize(right * (u * ratio * fov) + true_up * (v * fov) + dir)
        };
    }
};
