#include "kdmesh.h"
#include "color.h"
#include "camera.h"
#include <iostream>

int main()
{
    KDMesh const mesh(importOBJ("../suzanne.obj"));

    auto const raycast = [&](Ray const ray) noexcept -> vec3
    {
        vec3 const albedo       = {0.8f , 0.8f , 0.8f };
        vec3 const skyColor     = {0.53f, 0.81f, 0.92f};
        vec3 const lightColor   = {1.f  , 0.98f, 0.88f};
        vec3 const lightDir     = normalize({1.f, 1.f, 0.2f});

        std::optional<KDMesh::Hit> const hit = mesh.closestHit(ray);
        if(!hit)
            return skyColor;

        auto const [pos, norm] = hit.value();
        Ray const shadowRay = {pos, lightDir};
        vec3 const color = !mesh.closestHit(shadowRay)
            ? lightColor * std::max(0.f, dot(norm, lightDir))
            : vec3{0.f, 0.f, 0.f};
        return (skyColor * 0.2f + color) * albedo;
    };

    unsigned int const width = 1920;
    unsigned int const height = 1080;

    float const phi = 0.3f;
    float const theta = 0.4f;
    Camera const camera =
    {
        1.f,
        static_cast<float>(width) / height,
        vec3
        {
            std::sin(phi) * std::cos(theta),
            std::sin(theta),
            std::cos(phi) * std::cos(theta)
        } * 0.7f,
        {0.f, 1.f, 0.f},
        {0.f, 0.f, 0.f},
    };

    std::ofstream out("result.ppm");
    out << "P6" << std::endl;
    out << width << " " << height << std::endl;
    out << "255" << std::endl;

    for(unsigned int py = 0; py < height; ++py)
        for(unsigned int px = 0; px < width; ++px)
        {
            float const fx = (2.f * static_cast<float>(px)) / width - 1.f;
            float const fy = (2.f * static_cast<float>(py)) / height - 1.f;
            Color const color = toRGB8(raycast(camera.cast(fx, -fy)), 0.8f);
            out.write(reinterpret_cast<char const *>(&color), sizeof(color));
        }
}
