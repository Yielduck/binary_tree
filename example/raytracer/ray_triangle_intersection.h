#pragma once
#include "ray.h"
#include "triangle.h"
struct RayTriangleIntersection
{
    float t;
    Triangle::Barycentric coord;

    RayTriangleIntersection(Ray const &ray, Triangle const &triangle) noexcept
    {
        // ray:      r = r.pos + r.dir * t
        // triangle: r = t0 + (t1 - t0) * u + (t2 - t0) * v
        // variables are t, u, v
        // after merge: -r.dir * t + (t1 - t0) * u + (t2 - t0) * v = r.pos - t0
        // in a shorter form: a * t + b * u + c * v = d
        vec3 const b = triangle.b;
        vec3 const c = triangle.c;
        vec3 const d = ray.pos - triangle.p0;
        float const det0 = dot(ray.dir, cross(b, c));
        float const det1 = dot(d,       cross(b, c));
        float const det2 = dot(ray.dir, cross(d, c));
        float const det3 = dot(ray.dir, cross(b, d));
        t = -det1 / det0;
        coord = {det2 / det0, det3 / det0};
    }
};
