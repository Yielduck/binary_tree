#pragma once
#include "vec3.h"
#include <binary_tree/construct.h>
#include <algorithm>

struct KDTree
{
    struct AABB
    {
        vec3 pmin;
        vec3 pmax;
    };

    std::unique_ptr<AABB[]> aabb;
    std::unique_ptr<binary_tree::node[]> tree;

    KDTree() = default;
    template<typename Iterator>
    KDTree(Iterator const bbegin, Iterator const bend)
        : aabb(new AABB[2 * (bend - bbegin) - 1])
        , tree(binary_tree::construct
        (
            static_cast<binary_tree::idx_t>(bend - bbegin),
            [this, bbegin](binary_tree::idx_iter const ibegin, binary_tree::idx_iter const iend, binary_tree::idx_t const curr) noexcept
            {
                // assemble [ibegin, iend) AABB, store it in the aabb array
                AABB &box = aabb[curr];
                box.pmin = bbegin[*ibegin].pmin;
                box.pmax = bbegin[*ibegin].pmax;
                for(binary_tree::idx_iter it = ibegin + 1; it != iend; ++it)
                    for(unsigned int dim = 0; dim < 3; ++dim)
                    {
                        box.pmin[dim] = std::min(box.pmin[dim], bbegin[*it].pmin[dim]);
                        box.pmax[dim] = std::max(box.pmax[dim], bbegin[*it].pmax[dim]);
                    }
                // sort axes
                float const dx[3] =
                {
                    box.pmax[0] - box.pmin[0],
                    box.pmax[1] - box.pmin[1],
                    box.pmax[2] - box.pmin[2],
                };
                unsigned int axle[3];
                axle[0] = dx[0] > dx[1] ? (dx[0] > dx[2] ? 0u : 2u) : (dx[1] > dx[2] ? 1u : 2u); // longest
                axle[2] = dx[0] > dx[1] ? (dx[1] > dx[2] ? 2u : 1u) : (dx[0] > dx[2] ? 2u : 0u); // shortest
                axle[1] = 3u - axle[0] - axle[2];
                // select median point
                auto const less = [bbegin, axle](binary_tree::idx_t const idx1, binary_tree::idx_t const idx2) noexcept
                {
                    AABB const aabb1 = bbegin[idx1];
                    AABB const aabb2 = bbegin[idx2];
                    for(unsigned int i = 0; i < 3; ++i)
                    {
                        float const mid1 = 0.5f * (aabb1.pmin[axle[i]] + aabb1.pmax[axle[i]]);
                        float const mid2 = 0.5f * (aabb2.pmin[axle[i]] + aabb2.pmax[axle[i]]);
                        if(mid1 != mid2)
                            return mid1 < mid2;
                    }
                    return false; // equal
                };
                binary_tree::idx_iter const imiddle = ibegin + (iend - ibegin) / 2;
                std::nth_element(ibegin, imiddle, iend, less);
                return imiddle;
            }
        ))
    {
        for(binary_tree::idx_t i = 0; i < 2 * (bend - bbegin) - 1; ++i)
        {
            binary_tree::node const node = tree[i];
            if(node.state == binary_tree::node::leaf)
                aabb[i] = bbegin[node.idx()];
        }
    }
};
