#pragma once
#include "vec3.h"
struct Triangle
{
    vec3 p0;
    vec3 b, c;

    Triangle() = default;
    Triangle(vec3 const v0, vec3 const v1, vec3 const v2) noexcept
        : p0(v0)
        , b(v1 - v0)
        , c(v2 - v0)
    {}
    struct Barycentric
    {
        float b, c;
        bool valid() const noexcept {return b >= 0.f && c >= 0.f && b + c <= 1.f;}
    };
    vec3 interpolate(Barycentric const i) const noexcept
    {
        return p0 + b * i.b + c * i.c;
    }
};
