#include <binary_tree/construct.h>
#include <algorithm>
#include <array>
#include <iostream>
int main()
{
    unsigned int const pow2 = 16;
    unsigned int const N = 1 << pow2;
    std::array<int, N> a;
    for(int &i : a)
        i = std::rand();

    std::unique_ptr<binary_tree::node[]> const tree =
        binary_tree::construct(N, [&a](binary_tree::idx_iter const begin, binary_tree::idx_iter const end, binary_tree::idx_t) noexcept -> binary_tree::idx_iter
        {
            auto const less = [&a](binary_tree::idx_t const i, binary_tree::idx_t const j) noexcept -> bool
            {
                return a[i] < a[j];
            };
            binary_tree::idx_iter const middle = begin + (end - begin) / 2;
            std::nth_element(begin, middle, end, less);
            return middle;
        });

    std::size_t depth = 0;
    std::array<binary_tree::idx_t, pow2 + 1> stack;

    // push root node
    stack[++depth] = 0;
    while(depth != 0)
    {
        binary_tree::idx_t const i = stack[--depth];
        if(tree[i].state == binary_tree::node::parent)
        {
            // left child should be traversed earlier: put it on the top of the stack
            stack[depth++] = tree[i].right();
            stack[depth++] = tree[i].left();
        }
        else
            std::cout << a[tree[i].idx()] << std::endl;
    }
}
